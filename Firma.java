package zadanieFirma;

import java.math.BigDecimal;

/**
 * Created by Robert on 2017-11-11.
 */
public class Firma {
    public static void main(String[] args) {

        Pracownik pracownik = new Pracownik();
        pracownik.setIloscGodzin(180);
        pracownik.setStawkaGodzinowa(BigDecimal.valueOf(12));

        Menager menager = new Menager(150, BigDecimal.valueOf(25.0), BigDecimal.valueOf(200.0));
        menager.work();
        menager.getSalary();
        menager.dinnerTime();

        Prezes prezes = new Prezes(120, BigDecimal.valueOf(40.0), BigDecimal.valueOf(350.0));
        prezes.work();
        prezes.getSalary();
        prezes.dinnerTime();

        Magazynier magazynier = new Magazynier(180,BigDecimal.valueOf(12.76));
        magazynier.work();
        magazynier.getSalary();
        magazynier.dinnerTime();



    }
}
