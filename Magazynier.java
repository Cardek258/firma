package zadanieFirma;

import java.math.BigDecimal;

/**
 * Created by Robert on 2017-11-11.
 */
public class Magazynier extends Pracownik {

    public Magazynier(int iloscGodzin, BigDecimal stawkaGodzinowa){

        super(iloscGodzin, stawkaGodzinowa, BigDecimal.valueOf(0));
    }

    public void work(){
        System.out.print("Magazynier zajmuje się magazynierowaniem i zarabia: ");
    }

}
