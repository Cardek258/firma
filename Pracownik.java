package zadanieFirma;

import java.math.BigDecimal;

/**
 * Created by Robert on 2017-11-11.
 */
public class Pracownik {

    private int iloscGodzin;
    private BigDecimal stawkaGodzinowa;
    private BigDecimal bonus;

    public Pracownik(int iloscGodzin, BigDecimal stawkaGodzinowa, BigDecimal bonus) {
        this.iloscGodzin = iloscGodzin;
        this.stawkaGodzinowa = stawkaGodzinowa;
        this.bonus = bonus;
    }
    public Pracownik(int iloscGodzin, BigDecimal stawkaGodzinowa){
        this.iloscGodzin = iloscGodzin;
        this.stawkaGodzinowa = stawkaGodzinowa;

    }

    public Pracownik() {

    }

    public int getIloscGodzin() {
        return iloscGodzin;
    }

    public void setIloscGodzin(int iloscGodzin) {
        this.iloscGodzin = iloscGodzin;
    }

    public BigDecimal getStawkaGodzinowa() {
        return stawkaGodzinowa;
    }

    public void setStawkaGodzinowa(BigDecimal stawkaGodzinowa) {
        this.stawkaGodzinowa = stawkaGodzinowa;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public void getSalary() {
        System.out.println(BigDecimal.valueOf(iloscGodzin).multiply(stawkaGodzinowa).add(bonus));
    }

    public void dinnerTime(){
        System.out.println("Przerwe obiadowa ma o godz 14");
    }


}
