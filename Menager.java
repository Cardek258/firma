package zadanieFirma;

import java.math.BigDecimal;

/**
 * Created by Robert on 2017-11-11.
 */
public class Menager extends Pracownik {



    public Menager(int iloscGodzin, BigDecimal stawkaGodzinowa, BigDecimal bonus){
        super(iloscGodzin, stawkaGodzinowa, bonus);
    }

    public void work(){
        System.out.print("Menager zajmuje się menagerowaniem i zarabia: ");
    }

    @Override
    public void dinnerTime() {
        System.out.println("Przerwe obiadowa ma w czasie wolnym.");
    }

}
