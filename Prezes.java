package zadanieFirma;

import java.math.BigDecimal;

/**
 * Created by Robert on 2017-11-11.
 */
public class Prezes extends Pracownik {

    public Prezes(int iloscGodzin, BigDecimal stawkaGodzinowa, BigDecimal bonus){

        super(iloscGodzin, stawkaGodzinowa, bonus);
    }

    public void work(){
        System.out.print("Prezes zajmuje się prezesowaniem i zarabia: ");
    }

    @Override
    public void dinnerTime() {
        System.out.println("Przerwe obiadowa ma w czasie wolnym.");
    }
}
